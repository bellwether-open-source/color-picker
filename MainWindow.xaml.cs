﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ColorPicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            SliderRed.ValueChanged += (_unused1, _unused2) => HandleColorChange();
            SliderGreen.ValueChanged += (_unused1, _unused2) => HandleColorChange();
            SliderBlue.ValueChanged += (_unused1, _unused2) => HandleColorChange();
        }

        private void HandleColorChange()
        {
            /*
             * Colors are composed of Red, Green, and Blue values, each should be an integer ranging from 0-255.
             * Each value should be treated as a byte.  However, that's not how the Slider works; its values are stored as double.
             * The below code is broken because we're trying to shoehorn a double into a byte, but we're missing a cast.
             */
            byte red = (byte)SliderRed.Value;
            byte green = (byte)SliderGreen.Value;
            byte blue = (byte)SliderBlue.Value;
            RedValue.Content = $"({red:X})";
            GreenValue.Content = $"({green:X})";
            BlueValue.Content = $"({blue:X})";
            
            // Need to set background color on ColorRect
            // See: https://docs.microsoft.com/en-us/dotnet/desktop/wpf/graphics-multimedia/how-to-paint-an-area-with-a-solid-color?view=netframeworkdesktop-4.8
            var brush = new SolidColorBrush();

            brush.Color = Color.FromRgb(red, green, blue);

            ColorRect.Fill = brush;

            // Update HexValue text to show the "hex code" (#<red in hex><green in hex><blue in hex>), e.g. "#0a00ff"
            // This will rely on string formatting; see https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-numeric-format-strings
            HexValue.Text = $"#{red:X2}{green:X2}{blue:X2}";
        }
    }
}
